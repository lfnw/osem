// @license magnet:?xt=urn:btih:5305d91886084f776adcf57509a648432709a7c7&dn=x11.txt
$(function () {

    WebFont.load({
      google: {
        families: ['Arvo:700', 'Ubuntu:400']
      }
    });

});
// @license-end
