// get current_date
var today = new Date().toISOString().slice(0, 10);
$(function () {
  $("input[id^='datetimepicker']").datetimepicker({
    pickTime: true,
    useCurrent: false,
    sideBySide: true,
    autoclose: true,
    format: 'YYYY-MM-DD HH:mm'
  });

  $('.datetimepicker').datetimepicker({
    pickTime: true,
    useCurrent: false,
    sideBySide: true,
    format: 'YYYY-MM-DD HH:mm'
  });

   $("#conference-end-datepicker").datetimepicker({
       pickTime: false,
       useCurrent: false,
       format: "YYYY-MM-DD",
   });

   //   end_date_conference >= registration-period-Start_date >= Current_date
   //   registration-period-Start_date <= registration-period-End_date <= End_date (of conference)
   $("#registration-period-start-datepicker").datetimepicker({
       pickTime: false,
       useCurrent: false,
       format: "YYYY-MM-DD",
       minDate : today,
       maxDate : $("#registration-period-start-datepicker").attr('end_date'),
   });

   $("#registration-period-end-datepicker").datetimepicker({
       pickTime: false,
       useCurrent: false,
       format: "YYYY-MM-DD",
       minDate: today,
       maxDate : $("#registration-period-start-datepicker").attr('end_date'),
   });

  $("#conference-start-datepicker").on("dp.change",function (e) {
      $('#conference-end-datepicker').data("DateTimePicker").setMinDate(e.date);
      if (!$('#conference-end-datepicker').val()) {
         $('#conference-end-datepicker').data("DateTimePicker").setDate(e.date);
      }
  });

  $("#conference-start-datepicker").change(function (e) {
      $('#conference-start-datepicker').val()?$('#conference-end-datepicker').data("DateTimePicker").setMinDate(e.date):$('#conference-end-datepicker').data("DateTimePicker").setMinDate(null);
  });

  $("#conference-end-datepicker").on("dp.change",function (e) {
      $('#conference-start-datepicker').data("DateTimePicker").setMaxDate(e.date);
  });

  $("#conference-end-datepicker").change(function (e) {
      $('#conference-end-datepicker').val()?$('#conference-start-datepicker').data("DateTimePicker").setMaxDate(e.date):$('#conference-start-datepicker').data("DateTimePicker").setMaxDate(null);
  });

  $("#registration-period-start-datepicker").on("dp.change",function (e) {
      $('#registration-period-end-datepicker').data("DateTimePicker").setMinDate(e.date);
      if (!$('#registration-period-end-datepicker').val()) {
         $('#registration-period-end-datepicker').data("DateTimePicker").setDate(e.date);
      }
  });
  $("#registration-period-end-datepicker").on("dp.change",function (e) {
      $('#registration-period-start-datepicker').data("DateTimePicker").setMaxDate(e.date);
  });
} );
