# frozen_string_literal: true

module Admin
  class RegistrationsController < Admin::BaseController
    load_and_authorize_resource :conference, find_by: :short_title
    load_and_authorize_resource :registration, through: :conference
    before_action :set_user, except: [:index]

    def index
      registrations_order = case params[:order]
      when 'legal'
        'LOWER(users.legal_name) ASC, LOWER(users.display_name) ASC, LOWER(username) ASC'
      else
        'user_id ASC'
      end
      authorize! :show, Registration.new(conference_id: @conference.id)
      @pdf_filename = "#{@conference.title}.pdf"
      @registrations = @conference.registrations.includes(:user).references(:user).order(registrations_order)
      @attended = @conference.registrations.where('attended = ?', true).count
      @tickets = Hash[@conference.tickets.collect{|t| [t.id, t]}]

      @registration_distribution = @conference.registration_distribution
      @affiliation_distribution = @conference.affiliation_distribution
      @code_of_conduct = @conference.code_of_conduct.present?
      @speaker_ids = @conference.program.speakers.confirmed.pluck('users.id')

      @ticket_ids = {}
      TicketPurchase.where(conference: @conference).paid.where(user_id: @registrations.collect(&:user_id)).pluck(:user_id, :ticket_id).sort.each do |pair|
        @ticket_ids[pair[0]] ||= []; @ticket_ids[pair[0]] << pair[1]
      end

      respond_to do |format|
        format.html
        format.json do
          render json: RegistrationDatatable.new(view_context, conference: @conference, ticket_ids: @ticket_ids, tickets: @tickets, speaker_ids: @speaker_ids)
        end
        format.csv { render layout: false }
        format.xlsx { render layout: false }
        format.pdf { render layout: false }
      end
    end

    def new
      # Redirect to registration edit when user is already registered
      if @conference.user_registered?(@user)
        # Authorization needs to happen in every action before the return statement
        # We authorize the #edit action, since we redirect to it
        @registration = @user.registrations.find_by(conference_id: @conference.id)
        authorize! :edit, @registration
        redirect_to edit_admin_conference_registration_path(@conference, @registration)
      end
      @registration = @conference.registrations.new(user: @user)
    end

    def create
      @user.update_attributes(user_params)
      @conference.registrations.new(registration_params)

      if @registration.save
        redirect_to admin_conference_registrations_path(@conference),
          notice: "#{@user.name} is now registered for #{@conference.title}."
      else
        redirect_back fallback_location: admin_conference_registrations_path(@conference),
          error: "#{@user.name} was not registered to #{@conference.title}: #{@registration.errors.full_messages.to_sentence}"
      end
    end

    def edit; end

    def update
      @user.update_attributes(user_params)

      @registration.update_attributes(registration_params)
      if @registration.save
        redirect_to admin_conference_registrations_path(@conference.short_title),
                    notice: "Successfully updated registration for #{@registration.user.email}!"
      else
        flash.now[:error] = "An error prohibited the Registration for #{@registration.user.email}: "\
                        "#{@registration.errors.full_messages.join('. ')}."
        render :edit
      end
    end

    def destroy
      if can? :destroy, @registration
        @registration.destroy
        redirect_to admin_conference_registrations_path(@conference.short_title),
                    notice: "Deleted registration for #{@user.name}!"
      else
        redirect_to admin_conference_registrations_path(@conference.short_title),
                    error: 'You must be an admin to delete a registration.'
      end
    end

    def toggle_attendance
      @registration.attended = !@registration.attended
      if @registration.save
        head :ok
      else
        head :unprocessable_entity
      end
    end

    def toggle_code_of_conduct
      @registration.accepted_code_of_conduct = !@registration.accepted_code_of_conduct
      if @registration.save
        head :ok
      else
        head :unprocessable_entity
      end
    end

    private

    def set_user
      @user = User.find_by(id: params['user_id'] || @registration.user_id )
    end

    def user_params
      params.require(:user).permit(:legal_name, :display_name, :affiliation)
    end

    def registration_params
      params.require(:registration).permit(
        :user_id, :conference_id, :attended,
        :volunteer, :other_special_needs, :accepted_code_of_conduct,
        vchoice_ids: [], qanswer_ids: [], qanswers_attributes: [], event_ids: []
      )
    end
  end
end
