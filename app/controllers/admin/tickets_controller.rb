# frozen_string_literal: true

module Admin
  class TicketsController < Admin::BaseController
    load_and_authorize_resource :conference, find_by: :short_title
    load_and_authorize_resource :ticket, through: :conference

    def index
      authorize! :update, Ticket.new(conference_id: @conference.id)
      @tickets_sold_distribution = @conference.tickets_sold_distribution
      @tickets_turnover_distribution = @conference.tickets_turnover_distribution
    end

    def materials
      user_ids = @conference.ticket_purchases.paid.pluck(:user_id).uniq.sort
      registrations = @conference.registrations.includes(:user).references(:user)
      purchases = @conference.ticket_purchases.paid.includes(:ticket, :user).references(:ticket, :user)
      speaker_ids = @conference.program.speakers.confirmed.pluck('users.id')

      display_name_by_reg_id = {}
      registrations.each do |reg|
        display_name_by_reg_id[reg.id] = reg.user.name
      end

      purchases_by_reg_id = {}
      purchases.each do |purchase|
        user_id = purchase.user.id
        registration_id = registrations.find{|r| r.user_id == user_id }&.id
        purchases_by_reg_id[registration_id] ||= []
        purchases_by_reg_id[registration_id] << "#{purchase.quantity}×(#{purchase.ticket.materials&.join(',')})"
        if speaker_ids.include?(user_id)
          purchases_by_reg_id[registration_id] << 'speaker bag'
          speaker_ids.delete(user_id)
        end
      end
      unregistered_speakers = []
      speaker_ids.each do |speaker_id|
        registration_id = registrations.find{|r| r.user_id == speaker_id }&.id
        if registration_id
          purchases_by_reg_id[registration_id] ||= []
          purchases_by_reg_id[registration_id] << 'speaker bag'
        else
          unregistered_speakers << speaker_id
        end
      end

      @purchase_array = [
        [
          'badge_id',
          'display_name',
          'materials'
        ]
      ]
      purchases_by_reg_id.keys.sort{|a, b| a.to_s <=> b.to_s }.each do |reg_id|
        line = [
          reg_id,
          display_name_by_reg_id[reg_id],
          purchases_by_reg_id[reg_id].join(' ')
        ]
        @purchase_array << line
      end

      # render layout: false
    end

    def import
      Conference.find_by_short_title(import_from).tickets.each do |ticket|
        new_ticket = ticket.dup
        new_ticket.conference = @conference
        new_ticket.save!
      end
      flash[:notice] = "Imported #{@conference.tickets.count} tickets from '#{params[:from]}'"
      redirect_to admin_conference_tickets_path(@conference)
    rescue ActiveRecord::ActiveRecordError => e
      flash[:error] = e
    end

    def show
      @buyers = @ticket.buyers.eager_load(:ticket_purchases)
    end

    def new
      @ticket = @conference.tickets.new
    end

    def create
      @ticket = @conference.tickets.new(ticket_params)
      if @ticket.save(ticket_params)
        redirect_to admin_conference_tickets_path(conference_id: @conference.short_title),
                    notice: 'Ticket successfully created.'
      else
        flash.now[:error] = "Creating Ticket failed: #{@ticket.errors.full_messages.join('. ')}."
        render :new
      end
    end

    def edit; end

    def update
      if @ticket.update_attributes(ticket_params)
        redirect_to admin_conference_tickets_path(conference_id: @conference.short_title),
                    notice: 'Ticket successfully updated.'
      else
        flash.now[:error] = "Ticket update failed: #{@ticket.errors.full_messages.join('. ')}."
        render :edit
      end
    end

    def give
      ticket_purchase = @ticket.ticket_purchases.new(gift_ticket_params)
      recipient = ticket_purchase.user
      if ticket_purchase.save
        redirect_to(
          admin_conference_ticket_path(@conference.short_title, @ticket),
          notice: "#{recipient.name} was given a #{@ticket.title} ticket."
        )
      else
        redirect_back(
          fallback_location: admin_conference_ticket_path(@conference.short_title, @ticket),
          error:             "Unable to give #{recipient.name} a #{@ticket.title} ticket: " +
                             ticket_purchase.errors.full_messages.to_sentence
        )
      end
    end

    def destroy
      if @ticket.destroy
        redirect_to admin_conference_tickets_path(conference_id: @conference.short_title),
                    notice: 'Ticket successfully deleted.'
      else
        redirect_to admin_conference_tickets_path(conference_id: @conference.short_title),
                    error: 'Deleting ticket failed! ' \
                    "#{@ticket.errors.full_messages.join('. ')}."
      end
    end

    private

    def ticket_params
      params.require(:ticket).permit(
        :conference, :conference_id,
        :title, :url, :description,
        :price_cents, :price_currency, :price,
        :registration_ticket, :visible, :rank,
        :badge_ribbon, materials: []
      )
    end

    def gift_ticket_params
      response = params.require(:ticket_purchase).permit(
        :user_id
      )
      response.merge(paid: true, amount_paid: 0, conference: @conference)
    end

    def import_from
      params.require(:from)
    end
  end
end
