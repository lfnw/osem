# frozen_string_literal: true

class SchedulesController < ApplicationController
  load_and_authorize_resource
  protect_from_forgery with: :null_session
  before_action :respond_to_options
  load_resource :conference, find_by: :short_title
  load_resource :program, through: :conference, singleton: true, except: :index
  before_action :load_withdrawn_event_schedules, only: [:show, :events]

  def show
    event_schedules = @program.selected_event_schedules(
      includes: [{ event: %i[event_type speakers submitter] }]
    )

    if event_schedules.empty?
      redirect_to events_conference_schedule_path(@conference.short_title)
      return
    end

    @rooms = @conference.venue.rooms.order(:name) if @conference.venue
    @dates = @conference.start_date..@conference.end_date
    @step_minutes = @program.schedule_interval.minutes
    @conf_start = @conference.start_hour
    @conf_period = @conference.end_hour - @conf_start

    # the schedule takes you to today if it is a date of the schedule
    @current_day = @conference.current_conference_day
    @day = @current_day.present? ? @current_day : @dates.first
    if @current_day
      # the schedule takes you to the current time if it is beetween the start and the end time.
      @hour_column = @conference.hours_from_start_time(@conf_start, @conference.end_hour)
    end
    # Ids of the @event_schedules of confrmed self_organized tracks along with the selected_schedule_id
    @selected_schedules_ids = [@conference.program.selected_schedule_id]
    @conference.program.tracks.self_organized.confirmed.each do |track|
      @selected_schedules_ids << track.selected_schedule_id
    end
    @selected_schedules_ids.compact!
    @event_schedules_by_room_id = event_schedules.select { |s| @selected_schedules_ids.include?(s.schedule_id) }.group_by(&:room_id)

    @start_times = Hash[
      @dates.collect do |conf_day|
        [
          conf_day,
          event_schedules.select do |schedule|
            schedule.start_time.to_date == conf_day
          end.min do |schedule|
            schedule.start_time
          end.start_time
        ]
      end
    ]

    @stop_times = Hash[
      @dates.collect do |conf_day|
        [
          conf_day,
          event_schedules.select do |schedule|
            schedule.start_time.to_date == conf_day
          end.max do |schedule|
            schedule.end_time
          end.end_time
        ]
      end
    ]

    @rooms_by_day = Hash[
      @dates.collect do |conf_day|
        [
          conf_day,
          event_schedules.select do |schedule|
            schedule.start_time.to_date == conf_day
          end.collect{ |schedule| schedule.room_id }.uniq
        ]
      end
    ]
  end

  def events
    @dates = @conference.start_date..@conference.end_date
    @events_schedules = @program.selected_event_schedules(
      includes: [:room, { event: %i[track event_type speakers submitter difficulty_level] }]
    ).sort_by! do |event_schedule|
      [ event_schedule.start_time, event_schedule.room.name ]
    end
    @events_schedules = [] unless @events_schedules

    @unscheduled_events = @program.events.confirmed.eager_load(:speakers).order('users.display_name ASC') - @events_schedules.map(&:event)

    if @events_schedules.map(&:event).empty?
      @dates = []
    end

    # day = @conference.current_conference_day
    # @tag = day.strftime('%Y-%m-%d') if day
    respond_to do |format|
      format.html
      format.text { render layout: false }
      format.xlsx { render layout: false }
    end
  end

  def print
    @dates = (@conference.start_date..@conference.end_date).to_a
    @events_schedules = @program.selected_event_schedules(
      includes: [:room, { event: %i[track event_type speakers submitter difficulty_level] }]
    )
    @sponsorship_levels = @conference.sponsorship_levels.order(:position).where("position <= ?", 5).includes(:sponsors).references(:sponsors)
    render layout: 'print'
  end

  def kiosk
    event_schedules = @program.selected_event_schedules.sort_by! do |event_schedule|
      [ event_schedule.start_time, event_schedule.room.name ]
    end
    if Rails.env.development?
      @current_datetime = event_schedules[3].start_time
    else
      @current_datetime = DateTime.now
    end
    @now_event_schedules = event_schedules.select do |event_schedule|
      event_schedule.start_time <= @current_datetime &&
      event_schedule.end_time >= @current_datetime
    end
    @next_event_schedules = event_schedules.select do |event_schedule|
      event_schedule.start_time > @current_datetime
    end[0..19]
    render layout: 'kiosk'
  end

  private

  def respond_to_options
    respond_to do |format|
      format.html { head :ok }
    end if request.options?
  end

  def load_withdrawn_event_schedules
    # Avoid making repetitive EXISTS queries for these later.
    # See usage in EventsHelper#canceled_replacement_event_label
    @withdrawn_event_schedules = EventSchedule.withdrawn_or_canceled_event_schedules(@program.schedule_ids)
  end
end
