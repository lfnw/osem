# frozen_string_literal: true

class EventDatatable < AjaxDatatablesRails::Base
  def_delegator :@view, :admin_conference_program_event_path
  def_delegator :@view, :admin_user_path
  def_delegator :@view, :edit_admin_conference_program_event_path
  def_delegator :@view, :new_admin_conference_survey_path
  def_delegator :@view, :event_switch_checkbox
  def_delegator :@view, :event_type_dropdown
  def_delegator :@view, :track_dropdown
  def_delegator :@view, :difficulty_dropdown
  def_delegator :@view, :state_dropdown

  def view_columns
    @view_columns ||= {
      id: { source: 'Event.id', cond: :eq },
      title: { source: 'Event.title' },
      rating: { source: 'Vote.rating', searchable: false },
      speakers: { source: 'User.display_name' },
      require_registration: { source: 'Event.require_registration', searchable: false },
      highlight: { source: 'Event.is_highlight', searchable: false },
      event_type: { source: 'EventType.title' },
      track: { source: 'Track.name' },
      difficulty_level: { source: 'DifficultyLevel.title' },
      state: { source: 'Event.state' },
      url: { source: 'Registration.id', searchable: false, orderable: false },
      actions: { source: 'Registration.id', searchable: false, orderable: false }
    }
  end

  def data
    records.map do |record|
      {
        id: record.id,
        title: record.title,
        max_rating: conference.program.rating,
        average_rating: conference.program.show_voting? ? record.average_rating : false,
        user_rating: record.votes.first{ |vote| vote.user_id == user.id }&.rating || false,
        submitter: { name: record.submitter.name, url: admin_user_path(record.submitter) },
        speakers: record.speakers.map{ |speaker| { name: speaker.name, url: admin_user_path(speaker) } },
        require_registration: {
          value: record.require_registration,
          dom: event_switch_checkbox(record, :require_registration, conference.short_title)
        },
        highlight: {
          value: record.is_highlight,
          dom: event_switch_checkbox(record, :is_highlight, conference.short_title)
        },
        event_type: {
          value: record.event_type&.title,
          dom: event_type_dropdown(record, event_types, conference.short_title)
        },
        track: {
          value: record.track&.name,
          dom: track_dropdown(record, tracks, conference.short_title)
        },
        difficulty_level: {
          value: record.difficulty_level&.title,
          dom: difficulty_dropdown(record, difficulty_levels, conference.short_title)
        },
        state: {
          value: record.state,
          dom: state_dropdown(record, conference.short_title, conference.email_settings)
        },
        url: admin_conference_program_event_path(conference, record),
        actions: [
          { label: 'View', url: admin_conference_program_event_path(conference, record) },
          { label: 'Edit', url: edit_admin_conference_program_event_path(conference, record) },
          { label: 'Add survey', url: new_admin_conference_survey_path(conference, survey: { surveyable_type: 'Event', surveyable_id: record.id })}
        ],
        DT_RowId: record.id
      }
    end
  end

  private

  def user
    @user ||= options[:user]
  end

  def conference
    @conference ||= options[:conference]
  end

  def event_types
    @event_types ||= conference.program.event_types
  end

  def tracks
    @tracks ||= options[:tracks]
  end

  def difficulty_levels
    @difficulty_levels ||= conference.program.difficulty_levels
  end

  def get_raw_records # rubocop:disable Naming/AccessorMethodName
    conference.program.events.includes(:speakers, :votes, :event_type, :track, :difficulty_level).references(:event_users, :votes, :event_type, :track, :difficulty_level)
  end

  # override upstream santitation, which converts everything to strings
  def sanitize(records)
    records
  end
end
