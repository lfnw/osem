# frozen_string_literal: true

class EventVotingDatatable < AjaxDatatablesRails::Base
  def_delegator :@view, :admin_conference_program_event_path
  def_delegator :@view, :admin_user_path

  def view_columns
    @view_columns ||= {
      id:               { source: 'Event.id', cond: :eq },
      title:            { source: 'Event.title' },
      user_rating:      { source: 'my_rating', searchable: false },
      speakers:         { source: 'User.name' },
      event_type:       { source: 'EventType.title' },
      track:            { source: 'Track.name' },
      difficulty_level: { source: 'DifficultyLevel.title' },
      state:            { source: 'Event.state' }
    }
  end

  def data
    records.map do |record|
      {
        id: record.id,
        title: record.title,
        max_rating: conference.program.rating,
        user_rating: record.my_rating,
        speakers: record.speakers.map{ |speaker| { name: speaker.name, url: admin_user_path(speaker) } },
        event_type: record.event_type_title,
        track: record.track_name,
        difficulty_level: record.difficulty_level_title,
        url: admin_conference_program_event_path(conference, record),
        DT_RowId: record.id
      }
    end
  end

  def get_raw_records # rubocop:disable Naming/AccessorMethodName
    conference.program.events.joins("LEFT OUTER JOIN (select id, event_id, rating from votes where user_id = 2) as my_votes on my_votes.event_id = events.id").left_outer_joins(:event_type, :track, :difficulty_level).select("events.id, events.title, events.state, my_votes.rating as my_rating, event_types.title as event_type_title, tracks.name as track_name, difficulty_levels.title as difficulty_level_title").where('events.state' => 'new', 'tracks.submitter_id' => nil)
  end

  # ==== These methods represent the basic operations to perform on records
  # and feel free to override them

  # def filter_records(records)
  # end

  def sort_records(records)
    byebug
  end

  # def paginate_records(records)
  # end

  # ==== Insert 'presenter'-like methods below if necessary

  # override upstream santitation, which converts everything to strings
  def sanitize(records)
    records
  end

  def user
    @user ||= options[:user]
  end

  def conference
    @conference ||= options[:conference]
  end

  def event_types
    @event_types ||= conference.program.event_types
  end

  def tracks
    @tracks ||= options[:tracks]
  end

  def difficulty_levels
    @difficulty_levels ||= conference.program.difficulty_levels
  end
end
