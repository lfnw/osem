# frozen_string_literal: true

class RegistrationDatatable < AjaxDatatablesRails::Base
  def_delegator :@view, :edit_admin_conference_registration_path

  def view_columns
    @view_columns ||= {
      user_id:                  { source: 'Registration.user_id', cond: :eq},
      id:                       { source: 'Registration.id', cond: :eq },
      legal_name:               { source: 'User.legal_name' },
      display_name:             { source: 'User.display_name' },
      roles:                    { source: 'Role.name' },
      email:                    { source: 'User.email' },
      accepted_code_of_conduct: { source: 'Registration.accepted_code_of_conduct', searchable: false },
      speaker:                  { source: 'Registration.id', searchable: false, orderable: false },
      ticket_ribbons:           { source: 'Registration.id', searchable: false, orderable: false },
      actions:                  { source: 'Registration.id', searchable: false, orderable: false }
    }
  end

  private

  def conference
    @conference ||= options[:conference]
  end

  def speaker_ids
    @speaker_ids ||= options[:speaker_ids] || []
  end

  def ticket_ids
    @ticket_ids ||= options[:ticket_ids] || []
  end

  def tickets
    @tickets ||= options[:tickets] || []
  end

  def conference_role_titles(record)
    record.roles.collect do |role|
      role.name.titleize if role.resource == conference
    end.compact
  end

  def data
    records.map do |record|
      {
        user_id:                  record.user.id,
        id:                       record.id,
        legal_name:               record.user.legal_name,
        display_name:             record.user.name,
        roles:                    conference_role_titles(record.user),
        email:                    record.email,
        accepted_code_of_conduct: !!record.accepted_code_of_conduct, # rubocop:disable Style/DoubleNegation
        speaker:                  !!speaker_ids.include?(record.user_id), # rubocop:disable Style/DoubleNegation,
        ticket_ribbons:           ticket_ids[record.user_id]&.collect{|n| tickets[n].badge_ribbon }&.uniq&.sort&.join(' & '),
        edit_url:                 edit_admin_conference_registration_path(conference, record),
        DT_RowId:                 record.id
      }
    end
  end

  def get_raw_records # rubocop:disable Naming/AccessorMethodName
    conference.registrations.includes(user: :roles).references(:users, :roles).distinct
  end

  # override upstream santitation, which converts everything to strings
  def sanitize(records)
    records
  end
end
