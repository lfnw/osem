# frozen_string_literal: true

class Registration < ApplicationRecord
  require 'csv'
  belongs_to :user
  belongs_to :conference

  has_and_belongs_to_many :qanswers
  has_and_belongs_to_many :vchoices

  has_many :events_registrations
  has_many :events, through: :events_registrations, dependent: :destroy

  has_paper_trail ignore: %i(updated_at week), meta: { conference_id: :conference_id }

  accepts_nested_attributes_for :user
  accepts_nested_attributes_for :qanswers

  delegate :legal_name, to: :user
  delegate :email, to: :user
  delegate :display_name, to: :user
  delegate :affiliation, to: :user
  delegate :username, to: :user

  alias_attribute :other_needs, :other_special_needs

  validates :user, presence: true

  validates :user_id, uniqueness: { scope: :conference_id, message: 'already Registered!' }
  validate :registration_limit_not_exceed, on: :create
  validates :accepted_code_of_conduct, acceptance: {
    if: -> { conference.code_of_conduct.present? }
  }

  after_create :set_week, :subscribe_to_conference, :send_registration_mail

  ##
  # Makes a list of events that includes (in that order):
  # Events that require registration, and registration to them is still possible
  # Events to which the user is already registered to
  # ==== RETURNS
  # * +Array+ -> [event_to_register_to, event_already_registered_to]
  def events_ordered
    (conference.program.events.with_registration_open - events) + events
  end

  def name
    user.name
  end

  private

  def subscribe_to_conference
    Subscription.create(conference_id: conference.id, user_id: user.id)
  end

  def send_registration_mail
    if conference.email_settings.send_on_registration?
      Mailbot.registration_mail(self).deliver_later
    end
  end

  def set_week
    update!(week: created_at.strftime('%W'))
  end

  def registration_limit_not_exceed
    if conference.registration_limit > 0 && conference.registrations(:reload).count >= conference.registration_limit
      errors.add(:base, 'Registration limit exceeded')
    end
  end
end
