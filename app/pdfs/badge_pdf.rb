# frozen_string_literal: true

require 'open-uri'
require 'prawn/measurement_extensions'

class BadgePdf < Prawn::Document
  include FormatHelper

  VERSION = '0'
  MARGIN = 0.25.in
  STROKES = 'C0C0C0'
  DEBUG = false

  def initialize(registration, file_name, template=nil, custom_layout={})
    # OSEM data
    @registration = registration
    @user = @registration.user
    @conference = @registration.conference
    @tickets = @user.physical_tickets.by_conference(@conference)
    @ribbons = @user.badge_ribbons_for(@conference)
    # initialize
    layout = {
      page_layout:   :portrait,
      page_size:     'LETTER',
      print_scaling: :none,
      margin:        MARGIN,
      filename:      file_name,
      info:          {
        Author: 'LinuxFest Northwest',
        Title:  "#{@conference.title} Badge for #{@user.name}"
      }
    }.merge(custom_layout)
    super(layout)
    # add fonts
    font_families.update(
      'Ubuntu' => {
        normal:      Rails.root.join('app', 'assets', 'fonts', 'Ubuntu-R.ttf'),
        italic:      Rails.root.join('app', 'assets', 'fonts', 'Ubuntu-RI.ttf'),
        bold:        Rails.root.join('app', 'assets', 'fonts', 'Ubuntu-B.ttf'),
        bold_italic: Rails.root.join('app', 'assets', 'fonts', 'Ubuntu-BI.ttf')
      },
      'Open Sans' => {
        normal:      Rails.root.join('app', 'assets', 'fonts', 'OpenSans-Regular.ttf'),
        italic:      Rails.root.join('app', 'assets', 'fonts', 'OpenSans-Italic.ttf'),
        bold:        Rails.root.join('app', 'assets', 'fonts', 'OpenSans-Bold.ttf'),
        bold_italic: Rails.root.join('app', 'assets', 'fonts', 'OpenSans-BoldItalic.ttf')
      }
    )
    # default document font
    font('Open Sans', style: :normal)
    # default stroke style
    stroke_color(STROKES)
    # some helpers
    @left = bounds.left
    @right = bounds.right
    @top = bounds.top
    @bottom = bounds.bottom
    @image_path = Rails.root.join('app', 'assets', 'images')
    # find the template method
    template ||= @conference.short_title
    @template_method = if self.private_methods.include?("template_#{template}".to_sym)
      "template_#{template}".to_sym
    else
      :template_default
    end
    Rails.logger.debug "Ready to draw with template '#{@template_method}'"
    self.send(@template_method)
  end

  private

  def template_default
    template_2020
  end

  def template_2020
    # page is layed out as:
    # +-----------+------+
    # |   badge   | back |
    # +-----------+------+
    # | tickets   |      |
    # | sponsors  | CoC  |
    # +-----------+------+
    @mid_vertical = (bounds.top - 3.in + MARGIN) # 3" high badge, including top margin
    @mid_horizontal = (bounds.left + 4.in - MARGIN) # 4" wide badge
    @end_horizontal = (bounds.left + 8.in - MARGIN) # 2nd column
    stroke_axis if DEBUG
    #---- Badge ----
    bounding_box([@left, @top],
      width:  4.in - MARGIN*2,
      height: 3.in - MARGIN
    ) do
      # clip
      stroke do
        rounded_rectangle([bounds.width/2 - 0.3125.in, bounds.top], 0.625.in, 0.125.in, 0.0625.in)
      end
      # Logo
      svg IO.read(@image_path.join('lfnw-color-badge.svg')),
        at: [bounds.left, bounds.top],
        width: 1.1.in
      # display_name & affiliation
      bounding_box([bounds.left, bounds.top - 1.2.in],
        width: bounds.width,
        height: (bounds.top - 1.5.in)
      ) do
        font('Ubuntu') do
          text(@user.name.to_s,
            size: 16,
            align: :center,
            style: :bold,
            disable_wrap_by_char: true,
            overflow: :wrap
          )
          move_down(0.1.in)
          text(@user.affiliation.to_s,
            size: 11,
            align: :center,
            disable_wrap_by_char: true,
            overflow: :shrink_to_fit
          )
        end
        stroke_bounds if DEBUG
      end
      # ribbons
      @ribbons.each_with_index do |ribbon, index|
        break if index > 3
        at_height = (bounds.top - (index + 1)*0.22.in)
        badge_marker(ribbon, at_height)
      end
      stroke_bounds if DEBUG
      # registration id
      stroke_line([bounds.left, (bounds.bottom + 0.3.in)], [bounds.right, (bounds.bottom + 0.3.in)])
      bounding_box([(bounds.width - 0.5.in),bounds.bottom + 0.25.in], width: 0.5.in, height: 0.25.in) do
        font('Courier', size: 8) do
          icon "<icon size='8'>far-id-badge</icon> #{@registration.id}", inline_format: true, position: :right
        end
        stroke_bounds if DEBUG
      end
    end
    #---- Back ----
    bounding_box([(@mid_horizontal+MARGIN), @top],
    width:  4.in - MARGIN*2,
    height: 3.in - MARGIN
    ) do
      # clip
      stroke do
        rounded_rectangle([bounds.width/2 - 0.3125.in, bounds.top], 0.625.in, 0.125.in, 0.0625.in)
      end
      # Logo
      svg IO.read(@image_path.join('lfnw-color-badge.svg')),
        at: [bounds.right - 1.1.in, bounds.top],
        width: 1.1.in
      # display_name & affiliation
      bounding_box([bounds.left, bounds.top - 1.2.in],
        width: bounds.width,
        height: (bounds.top - 1.5.in)
      ) do
        font('Ubuntu') do
          text(@user.name.to_s,
            size: 16,
            align: :center,
            style: :bold,
            disable_wrap_by_char: true,
            overflow: :shrink_to_fit
          )
          move_down(0.1.in)
          text(@user.affiliation.to_s,
            size: 11,
            align: :center,
            disable_wrap_by_char: true,
            overflow: :shrink_to_fit
          )
        end
        stroke_bounds if DEBUG
      end
      # QR code - MECARD
      print_qr_code(@user.mecard,
        pos: [bounds.left - 0.1.in, bounds.top + 0.1.in],
        extent: 1.2.in,
        stroke: DEBUG,
        level: :q
      )
      # version & registration_id
      bounding_box(
        [bounds.left, bounds.bottom + 0.3.in],
        width: bounds.width,
        height: 0.2.in
      ) do
        font('Courier') do
          text_box("#{@template_method} v#{VERSION}",
            align: :left, valign: :bottom,
            size: 6
          )
          text_box("Reg ##{@registration.id}",
            align: :right, valign: :bottom,
            size: 8
          )
        end
        stroke_line([bounds.left, bounds.top], [bounds.right, bounds.top])
        stroke_bounds if DEBUG
      end
      stroke_bounds if DEBUG
    end
    # left column, below badge
    bounding_box([bounds.left, bounds.top - 3.in], width: @mid_horizontal) do
      #---- Tickets ----
      # title
      headline('Purchased Tickets')
      # table
      font('Courier', size: 9) do
        table_data = [
          [{content: '<b>ID</b>', rowspan: 2}, "<font name='Open Sans'>Type of Ticket</font>"],
          ['Verification Token']
        ]
        @tickets.each do |pticket|
          table_data << [
            {content: "<b>#{pticket.id.to_s}</b>", rowspan: 2},
            "<font name='Open Sans'>#{pticket.ticket.title}</font>"
          ]
          table_data << [pticket.token]
        end
        table(
          table_data,
          width: 3.75.in,
          column_widths: {1 => 3.25.in},
          cell_style: { inline_format: true }
        )
      end
      move_down 12
      #---- Check-in Tips ----
      headline('LinuxFest Northwest - Tips')
      font('Open Sans', size: 9) do
        ul([
          "When you arrive, <b>DON'T WAIT IN LINE!</b> Grab a schedule, and head to a session or the expo hall.",
          "Bring your badge by the HELP! STATION to pick up a lanyard. If you forget your badge, don't worry, we'll hook you up.",
          "If you have <i>purchased tickets</i>, just drop by the SWAG STATION anytime before lunch. (You do want your lunch tickets, right?) We will need to see that little number in the bottom right corner of your badge.",
          "You don't have to wear a badge if you don't want to, but you'll need to show it (either <i>physically or digitally</i>) to get into after-hours social events.",
          "The QR code on the back of your badge is in MECARD format, it has your display name, affiliation, and email address. If you're concerned about your privacy, feel free to mark it out, or cover it with a sticker!"
        ])
      end
      stroke_bounds if DEBUG
    end
    #---- Sponsors ----
    # right column, below badges
    bounding_box([(@mid_horizontal + 0.25.in), (bounds.top - 3.in)], width: @mid_horizontal) do
      #---- Code of Conduct ----
      headline('Code of Conduct')
      text(markdown(@conference.code_of_conduct).gsub(/<\/?p>/, ''),
        align: :justify, inline_format: true, overflow: :shrink_to_fit
      )
      stroke_bounds if DEBUG
    end
    #---- Finish ----
    draw_lines
  end

  def headline(text, custom_options={})
    font('Ubuntu', style: :bold, size: 12) do
      options = {
        align: :center
      }.merge(custom_options)
      text(text, options)
      move_down(6)
    end
  end

  def ul(items=[])
    table_data = items.collect do |item|
      ['•', item.to_s]
    end
    table(
      table_data,
      width: bounds.width,
      column_widths: {0 => 0.125.in},
      cell_style: { inline_format: true, borders: [], padding: 1}
    )
  end

  def draw_lines
    stroke_color(STROKES)
    dash(2, space: 2)
    stroke_line([0, @mid_vertical], [bounds.width, @mid_vertical])
    stroke_line([@mid_horizontal, @mid_vertical], [@mid_horizontal, bounds.height])
    stroke_line([@end_horizontal, (@mid_vertical - 0.375.in)], [@end_horizontal, bounds.height])
    dash(2, space: 0)
    # vertical scissor icon
    bounding_box([(@end_horizontal - 0.125.in), @mid_vertical - 0.125.in], width: 0.25.in, height: 0.25.in) do
      rotate(90, origin: [bounds.width/2 , bounds.height/2]) do
        icon 'fas-cut', size: 0.25.in
      end
      stroke_bounds if DEBUG
    end
    # horizontal scissor icon
    bounding_box([(bounds.width - 0.25.in), @mid_vertical +0.125.in], width: 0.25.in, height: 0.25.in) do
      rotate(180, origin: [bounds.width/2 , bounds.height/2]) do
        icon 'fas-cut', size: 0.25.in
      end
      stroke_bounds if DEBUG
    end

  end

  def badge_marker(label, top, options = {})
    banner_height = 0.2.in
    inset = 4
    margin = 2

    fill_color(options[:banner_color] || '000000')

    bounding_box([(bounds.width/2), top], width: (bounds.width/2), height: banner_height) do
      fill_polygon(
        [0, 0],
        [bounds.width, 0],
        [bounds.width, bounds.height],
        [0, bounds.height],
        [inset, (bounds.height / 2)]
      )
      fill_color(options[:text_color] || 'ffffff')
      text_box(label, align: :center, valign: :center, at: [inset, bounds.top], width: (bounds.width - inset), height: (bounds.height), overflow: :shrink_to_fit, size: 10, style: :italic)
      # reset
      fill_color '000000'
      stroke_bounds if DEBUG
    end
  end
end
