prawn_document(force_download: true, filename: @pdf_filename, page_layout: :landscape) do |pdf|
  table_array = []
  header_array = [
    'id',
    'Legal Name',
    'Display Name',
    'username',
    'Email',
    'Category'
  ]

  table_array << header_array
  @registrations.each do |registration|
    ribbons = (@ticket_ids[registration.user_id]&.collect{|n| @tickets[n].badge_ribbon }) || []
    ribbons << ('Speaker' if @speaker_ids.include?(registration.user_id))
    ribbons = ribbons.compact.uniq.collect(&:downcase)
    category = case
    when ribbons.include?('speaker')
      'speaker (green)'
    when ribbons.include?('sponsor')
      'sponsor (orange)'
    when ribbons.include?('supporter')
      'supporter (yellow)'
    when registration.id <= 2275
      '(pink)'
    else
      ''
    end

    row = [
      registration.id,
      registration.user.legal_name,
      registration.user.display_name,
      registration.user.username,
      registration.user.email,
      category
    ]
    table_array << row
  end

  pdf.text "#{@conference.title} Registrations", font_size: 25
  pdf.table table_array, header: true, cell_style: {size: 8, border_width: 1, overflow: :shrink_to_fit}
end
