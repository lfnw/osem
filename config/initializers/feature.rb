require 'feature'

repo = Feature::Repository::SimpleRepository.new

# configure features here
unless(ENV['RECAPTCHA_SITE_KEY'].blank? || ENV['RECAPTCHA_SECRET_KEY'].blank?)
  repo.add_active_feature :recaptcha
end

if ENV['CLEANTALK_API_KEY'].present?
  repo.add_active_feature :cleantalk
end

Feature.set_repository repo
