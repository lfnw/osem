class ChangeUserNames < ActiveRecord::Migration[5.0]
  def change
    rename_column :users, :name, :legal_name
    rename_column :users, :nickname, :display_name
  end
end
