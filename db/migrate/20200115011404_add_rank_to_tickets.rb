class AddRankToTickets < ActiveRecord::Migration[5.0]
  def change
    add_column :tickets, :rank, :integer
    add_index :tickets, [:conference_id, :rank]
  end
end
