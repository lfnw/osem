class AddForumToContact < ActiveRecord::Migration[5.0]
  def change
    add_column :contacts, :forum, :string
  end
end
