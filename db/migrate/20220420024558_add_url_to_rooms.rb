class AddUrlToRooms < ActiveRecord::Migration[5.0]
  def change
    add_column :rooms, :url, :text
  end
end
